var express = require('express');
var serveStatic = require('serve-static');

var app = express();

app.use(serveStatic(__dirname + '/app'));
app.listen(3000, function(){
    console.log('Server started at http://localhost:3000');
})