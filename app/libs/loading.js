function loading(status, text) {
    if(text) {
        $('#modal-body > h4').text(text);
    } 

    if(status === 'start'){
        $('#modal-loading').modal('show');
    }

    if(status === 'stop'){
        $('#modal-loading').modal('hide');
    }
}