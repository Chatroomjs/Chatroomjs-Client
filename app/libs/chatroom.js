var Chatroom = function (options) {
    var defaultOptions = {
        server: 'http://localhost:8080',
    };
    this.config = options ? extend(options, defaultOptions) : defaultOptions;
    this.sessionId = null;
    this.sessionToken = null;
    this.apiKey = null;
}

Chatroom.prototype.signup = function (newuser, callback, loading) {
    post(this.config.server + '/signup', newuser, false, function (data, status) {
        if (status == 200) {
            callback(data);
        } else {
            callback(null, data);
            throw 'Error in request. Got ' + status + " with the message: " + data;
        }
    }, loading);
}

Chatroom.prototype.login = function (loginInfo, callback, loading) {
    post(this.config.server + '/login', loginInfo, false, function (data, status) {
        if (status == 200) {
            localStorage.setItem("authToken", data.token);
            callback(data.account)
        } else {
            callback(null, data)
            throw 'Error in request. Got ' + status + " with the message: " + data;
        }
    }, loading);
}

Chatroom.prototype.logout = function () {
    localStorage.removeItem("authToken");
}

Chatroom.prototype.join = function (roomName, callback, loading) {
    get(this.config.server + '/joinroom', roomName, true, function (data, status) {
        if (status == 200) {
            this.sessionId = data.sessionId;
            this.apiKey = data.apiKey;
            this.sessionToken = data.token;

            this.session = OT.initSession(this.apiKey, this.sessionId);
            this.session.connect(this.sessionToken, function (error) {
                if (error) {
                    return callback(null, error);
                }
                console.log('Session connected');
                callback('Connected to the session');
            })

        } else {
            callback(null, data);
        }
    }.bind(this), loading);
}

Chatroom.prototype.subscribe = function (DOMElement, callback) {
    if (!this.session) {
        throw 'You need to join a room first';
    }
    this.session.on('streamCreated', function (event) {
        console.log('New Stream in the Session ' + event);
        this.session.subscribe(event.stream, DOMElement, { insertMode: 'append' }, function (err) {
            if (err) {
                callback('Streaming connection failed. This could be due to a restrictive firewall.');
            }
            callback();
        });
        
    });
}

Chatroom.prototype.publish = function (DOMElement) {
    if (!this.session) {
        throw 'You need to join a room first';
    }
    this.session.publish(OT.initPublisher(DOMElement, {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }));
}

Chatroom.prototype.sendText = function (text) {
    if (!this.session) {
        throw 'You need to join a room first';
    }
    this.session.signal({ data: text }, function (err) {
        if (err) {
            throw 'Error sending the signal ' + err;
        }
    });
}

function get(url, params, authenticated, callback, loading) {
    var request = new XMLHttpRequest();
    request.open('GET', encodeURI(url + '/' + params), true);

    if (authenticated) {
        request.setRequestHeader('x-access-token', localStorage.getItem('authToken'));
    }

    request.onload = function () {
        if (loading) loading('stop');
        var data = JSON.parse(request.responseText);
        callback(data, request.status);
    };

    request.send();
    if (loading) loading('start');
}

function post(url, data, authenticated, callback, loading) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    if (authenticated) {
        request.setRequestHeader('x-access-token', localStorage.getItem('authToken'));
    }

    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = function () {
        if (loading) loading('stop');
        var data = JSON.parse(request.responseText);
        callback(data, request.status);
    }

    request.send(JSON.stringify(data));
    if (loading) loading('start');
}

function extend(opt, defaultConfig) {
    var config = {};

    for (var defProp in defaultConfig) {
        if (defaultConfig.hasOwnProperty(defProp) && opt[defProp]) {
            config[defProp] = opt[defProp];
        } else {
            config[defProp] = defaultConfig[defProp];
        }
    }

    return config;
};

var chatroom = new Chatroom();