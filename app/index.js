(function ($) {
    'use strict'

    if (localStorage.getItem('authToken')) {
        $('#nav-signin').hide();
        $('#nav-logout').show();

        $('#nav-subscribe').parent().removeClass('disabled');
        $('#nav-publish').parent().removeClass('disabled');

        $('.body-block').children().remove();
        $('.body-block')
            .append($('<strong>', { text: 'You\'re logged in! Your account is ' })
                .append($('<a>', { href: localStorage.getItem('accountLink'), text: 'here', target: '_blank' })));
    } else {
        $('#nav-signin').show();
        $('#nav-logout').hide();

        $('#nav-subscribe').parent().addClass('disabled');
        $('#nav-publish').parent().addClass('disabled');
    }

    $('#nav-signin').click(function (ev) {
        ev.preventDefault();
        $('#nav-signin').siblings().removeClass('active');
        $('#nav-signin').addClass('active');

        $.get('/pages/sign/signin.html', function (page) {
            $('.body-block').html(page);
        })
    });

    $('#nav-subscribe').click(function (ev) {
        ev.preventDefault();
        $('#nav-subscribe').siblings().removeClass('active');
        $('#nav-subscribe').addClass('active');

        if (!$(ev.target).parent().hasClass('disabled')) {
            $.get('/pages/subscribe/subscribe.html', function (page) {
                $('.body-block').html(page);
            })
        }
    });

    $('#nav-publish').click(function (ev) {
        ev.preventDefault();
        $('#nav-publish').siblings().removeClass('active');
        $('#nav-publish').addClass('active');

        if (!$(ev.target).parent().hasClass('disabled')) {
            $.get('/pages/publish/publish.html', function (page) {
                $('.body-block').html(page);
            })
        }
    });

    $('#nav-logout').click(function (ev) {
        ev.preventDefault();
        $('#nav-logout').siblings().removeClass('active');
        $('#nav-logout').addClass('active');

        chatroom.logout();
        window.location.reload();
    })

})(jQuery);