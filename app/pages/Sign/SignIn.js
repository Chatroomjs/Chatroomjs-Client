(function ($) {
    'use strict'

    $('#form-signup').submit(function (ev) {
        ev.preventDefault();
        var form = $(ev.target);
        var newUser = {
            givenName: $('[name="givenName"]', form).val(),
            surname: $('[name="surname"]', form).val(),
            username: $('[name="username"]', form).val(),
            email: $('[name="email"]', form).val(),
            password: $('[name="password"]', form).val(),
        }
        chatroom.signup(newUser, function (data, err) {
            if (err) {
                $('#alert-signup').text(err);  
                $('#alert-signup').show();  
            } else {
                $('#alert-success-signup').show();
            }
            console.log(data);
        }, loading);
    });

    $('#form-signin').submit(function (ev) {
        ev.preventDefault();
        var form = $(ev.target);
        var userInfo = {
            username: $('[name="username"]', form).val(),
            password: $('[name="password"]', form).val(),
        }

        chatroom.login(userInfo, function (data, err) {
            if (err) {
                $('#alert-signin').text(err);  
                $('#alert-signin').show();  
            } else {
                localStorage.setItem('accountLink', data);
                window.location.reload();
                console.log(data);
            }
        }, loading);

    });

})(jQuery)