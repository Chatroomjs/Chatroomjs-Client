(function ($) {
    'use strict'

    $('#subscribe').click(function () {
    });

    $('#form-subscribe').submit(function (ev) {
        ev.preventDefault();
        var form = $(ev.target);
        loading('start', 'Joining the room');
        chatroom.join($('[name="roomName"]', form).val(), function (data, err) {
            if(err) {
                if(err.message === 'Jwt is expired'){
                    alert('Your Session has expired!');
                    chatroom.logout();
                    window.location.reload();
                } else {
                    $('#alert-subscribed').text(err || err.message);
                    $('#alert-subscribed').show();
                    loading('stop');
                }
            } else {
                loading('stop');
                loading('start', 'Subscribing to the Stream');
                chatroom.subscribe('stream-subscribed', function(){
                    loading('stop');
                });
            }
        });
    });

    $('.btn-directional').click(function (ev) {
        ev.preventDefault();
        var cmd = ev.target.tagName.toUpperCase() === 'I' ? $(ev.target).parent().attr('data-value') : $(ev.target).attr('data-value');
        chatroom.sendText(cmd);

        console.log(cmd)
    })

})(jQuery)