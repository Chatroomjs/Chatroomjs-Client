(function ($) {
    'use strict'

    $('#form-publish').submit(function (ev) {
        ev.preventDefault();
        var form = $(ev.target);

        chatroom.join($('[name="roomName"]', form).val(), function (data, err) {
            if (err) {
                if (err.message === 'Jwt is expired') {
                    alert('Your Session has expired!');
                    chatroom.logout();
                    window.location.reload();
                } else {
                    $('#alert-publish').text(err.message);
                    $('#alert-publish').show();
                }
            } else {
                chatroom.publish(document.getElementById('publisher'));
            }
        });
    });

})(jQuery)