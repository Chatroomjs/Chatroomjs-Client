Pre-requisites:
Learn about stormpath
=====================

Sign up for Stormpath for free, if you haven’t already. They have a developer edition. 
Understand the basics
Read about stormpath and its data model
https://docs.stormpath.com/rest/product-guide/latest/about.html

Read a quick start about authentication using REST calls
https://docs.stormpath.com/rest/product-guide/latest/quickstart.html

How passport authentication works
https://docs.stormpath.com/rest/product-guide/latest/auth_n.html#how-password-authentication-works-in-stormpath


Learn about Opentok
====================
There is no forever free developer edition, but there is a free trial. You can sign up for free trial to start working. I own an api key, which I will share later.

Concepts - read about Streams, Connections, Sessions, Publishers, and Subscribers.
https://tokbox.com/developer/guides/core-concepts/

https://tokbox.com/developer/guides/


Check out a sample application using Opentok and understand the code flow.
===================================
https://github.com/opentok/OpenTokRTC


—— Actual Task ——
Call the project “Chatroomjs”

Nodejs Opentok Server - create REST endpoints for these
=====================
1)signup for new account

2) login with existing account

3) joinroom-. authentication required
If room not present then create new Opentok session and tag roomId with session 
For existing roomId find the session.
Connect to the Opentok session.

4) sendtext- authentication required
use signalling api to send string messages to all clients of the session


A custom client javascript library- call it “Chatroom.js” - implement these functions.
======================
1) Signup for new account-  use the REST endpoint in our node backend

2)login with existing account - authentication required - make the backend rest call.
if success then get the href of the account returned. 

3)Join room -  authentication required- make the backend rest call

4)Subscribing to a stream and show it on DOM id- authentication required

5)publishing to a stream and show it on DOM id- authentication required

6)use the signaling api to transmit text messages to all Opentok clients (use the Opentok web apis, don’t call the REST api )

Implement the javascript in such a way that other developers can use the js would out the knowdedge of  stormpath and Opentok 


Web pages ( use Chatroom.js methods for all pages)
=========
Signup and login windows (in the same page side by side).

Subscribe Stream - 
needs authentication - show remote camera’s feed.
Add four buttons on the page :

UP
LEFT	   
RIGHT
DOWN

Each button when clicked will send a text message by signalling api.     

Publish stream - needs authentication  - publish local camera’s feed.





